export type SignUpParams = {
  username: string;
  password: string;
};
