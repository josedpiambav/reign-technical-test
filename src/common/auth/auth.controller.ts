import { Body, Controller, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';

import { SignInParamsDTO } from './dtos/params/signIn.dto';
import { SignInResponseDTO } from './dtos/responses/signIn.dto';
import { SignUpParamsDTO } from './dtos/params/signUp.dto';
import { BadRequestResponseDTO } from './dtos/responses/badRequest.dto';
import { AuthService } from './auth.service';

@ApiTags('Auth')
@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Signup' })
  @ApiOkResponse({
    description: 'Signup',
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    type: BadRequestResponseDTO,
  })
  @Post('signup')
  singUp(@Body() { username, password }: SignUpParamsDTO): Promise<void> {
    return this.authService.signUp({ username, password });
  }

  @ApiOperation({ summary: 'Login' })
  @ApiOkResponse({
    description: 'Generated access token',
    type: SignInResponseDTO,
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    type: BadRequestResponseDTO,
  })
  @Post('signin')
  signIn(
    @Body() { username, password }: SignInParamsDTO,
  ): Promise<SignInResponseDTO> {
    return this.authService.signIn({ username, password });
  }
}
