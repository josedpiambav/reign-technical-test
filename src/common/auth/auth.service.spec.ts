import { BadRequestException } from '@nestjs/common';
import { NestApplication } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../../modules/users/users.service';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let app: NestApplication;
  let service: AuthService;

  const AuthDataMock = { username: 'test', password: 'test' };

  const UserMock = {
    id: 1,
    username: AuthDataMock.username,
    password: AuthDataMock.password,
  };

  const UsersServiceMock = {
    save: jest.fn(),
    find: jest.fn(),
    verifyPassword: jest.fn(),
  };

  const JwtServiceMock = {
    sign: jest.fn(),
  };

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [],
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: UsersServiceMock,
        },
        {
          provide: JwtService,
          useValue: JwtServiceMock,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);

    app = module.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('signUp', () => {
    it('Should call the right methods', async () => {
      await service.signUp(AuthDataMock);

      expect(UsersServiceMock.save).toHaveBeenCalledWith(AuthDataMock);
    });
  });

  describe('signIn', () => {
    beforeEach(() => {
      JwtServiceMock.sign.mockReturnValue('token');
    });

    it('Should call the right methods', async () => {
      UsersServiceMock.find.mockResolvedValue([UserMock]);

      UsersServiceMock.verifyPassword.mockReturnValue(true);

      const response = await service.signIn(AuthDataMock);

      expect(response).toEqual({ access_token: 'token' });
      expect(UsersServiceMock.find).toHaveBeenCalledWith({
        username: AuthDataMock.username,
      });
      expect(UsersServiceMock.verifyPassword).toHaveBeenCalledWith(
        UserMock,
        AuthDataMock.password,
      );
    });

    it('Should throw BadRequestException when not found user', async () => {
      UsersServiceMock.find.mockResolvedValue([]);

      try {
        await service.signIn(AuthDataMock);
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toEqual('User not exists');
      }
    });

    it('Should throw BadRequestException when password does not match', async () => {
      UsersServiceMock.find.mockResolvedValue([UserMock]);

      UsersServiceMock.verifyPassword.mockReturnValue(false);

      try {
        await service.signIn(AuthDataMock);
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toEqual('Bad credentials');
      }
    });
  });
});
