import * as request from 'supertest';
import { HttpStatus, ValidationPipe } from '@nestjs/common';
import { NestApplication } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

describe('AuthController', () => {
  let app: NestApplication;
  let controller: AuthController;

  const AuthServiceMock = {
    signUp: jest.fn(),
    signIn: jest.fn(),
  };

  const AuthDataMock = { username: 'test', password: 'test' };

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: AuthServiceMock,
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);

    app = module.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('POST /api/auth/signup', () => {
    it('Should call the right methods', async () => {
      const response = await request(app.getHttpServer())
        .post('/api/auth/signup')
        .send(AuthDataMock);

      expect(response.status).toEqual(HttpStatus.CREATED);
      expect(AuthServiceMock.signUp).toHaveBeenCalledWith(AuthDataMock);
    });
  });

  describe('POST /api/auth/signin', () => {
    it('Should call the right methods', async () => {
      const responseData = { access_token: 'token' };

      AuthServiceMock.signIn.mockResolvedValue(responseData);

      const response = await request(app.getHttpServer())
        .post('/api/auth/signin')
        .send(AuthDataMock);

      expect(response.body).toEqual(responseData);
      expect(response.status).toEqual(HttpStatus.CREATED);
      expect(AuthServiceMock.signIn).toHaveBeenCalledWith(AuthDataMock);
    });
  });
});
