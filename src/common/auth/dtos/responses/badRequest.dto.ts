import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class BadRequestResponseDTO {
  @ApiProperty({
    description: 'Status code',
    default: 400,
  })
  @IsNumber()
  @IsNotEmpty()
  statusCode: number;

  @ApiProperty({
    description: 'Message',
  })
  @IsString()
  @IsNotEmpty()
  message: string;

  @ApiProperty({
    description: 'Error',
    default: 'Bad Request',
  })
  @IsString()
  @IsNotEmpty()
  error: string;
}
