import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class SignInResponseDTO {
  @ApiProperty({
    description: 'Access token',
  })
  @IsString()
  @IsNotEmpty()
  access_token: string;
}
