import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { SignUpParams } from './types/params/signUp.type';
import { SignInParams } from './types/params/signIn.type';
import { SignInResponse } from './types/responses/signIn.type';
import { JwtPayload } from './types/jwtPayload.type';

import { UsersService } from '../../modules/users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  signUp({ username, password }: SignUpParams): Promise<void> {
    return this.usersService.save({ username, password });
  }

  async signIn({ username, password }: SignInParams): Promise<SignInResponse> {
    const userExists = await this.usersService.find({ username });

    if (!userExists.length) throw new BadRequestException('User not exists');

    const [user] = userExists;

    if (!this.usersService.verifyPassword(user, password))
      throw new BadRequestException('Bad credentials');

    const payload: JwtPayload = { username };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
