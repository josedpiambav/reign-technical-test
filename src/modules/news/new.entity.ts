import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity('news')
export class New {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  title: string;

  @Column({ nullable: true })
  url: string;

  @Column()
  author: string;

  @Column({ type: 'float', nullable: true, default: 0 })
  points: number;

  @Column({ type: 'text', nullable: true })
  story_text: string;

  @Column({ type: 'text', nullable: true })
  comment_text: string;

  @Column({ type: 'float', nullable: true, default: 0 })
  num_comments: number;

  @Column({ type: 'float', nullable: true })
  story_id: number;

  @Column({ nullable: true })
  story_title: string;

  @Column({ nullable: true })
  story_url: string;

  @Column({ type: 'float', nullable: true })
  parent_id: number;

  @Column({ type: 'float', nullable: true })
  created_at_i: number;

  @Column('text', { array: true })
  _tags: string[];

  @Column({ unique: true })
  objectID: string;

  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Column()
  @UpdateDateColumn()
  update_at: Date;

  @Column()
  @DeleteDateColumn()
  deleted_at: Date;
}
