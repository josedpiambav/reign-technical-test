import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { HourlyRetriveNewsTask } from './news.task';
import { New } from './new.entity';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([New]), ConfigModule],
  controllers: [NewsController],
  providers: [NewsService, HourlyRetriveNewsTask],
})
export class NewsModule {}
