import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';

import { NewsService } from './news.service';

@Injectable()
export class HourlyRetriveNewsTask {
  private readonly logger = new Logger(HourlyRetriveNewsTask.name);

  constructor(private newsService: NewsService) {}

  @Cron(CronExpression.EVERY_HOUR)
  async getHackerNews(): Promise<void> {
    this.logger.debug('Getting Data from API');
    await this.newsService.retriveFromAPI();
  }
}
