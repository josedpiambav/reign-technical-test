export type FindNewsParams = {
  id?: number;
  page?: number;
  author?: string;
  _tags?: string;
  month?: string;
  story_title?: string;
  objectID?: string;
  withDeleted?: boolean;
};
