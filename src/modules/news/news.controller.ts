import {
  Controller,
  Delete,
  Get,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiOkResponse,
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { FindNewsParamsDTO } from './dtos/params/findNews.dto';
import { FindNewsResponseDTO } from './dtos/responses/findNews.dto';
import { UnauthorizedResponseDTO } from './dtos/responses/unauthorizedResponse.dto';
import { NewsService } from './news.service';

@ApiUnauthorizedResponse({
  description: 'Invalid access token',
  type: UnauthorizedResponseDTO
})
@ApiBearerAuth('authorization')
@ApiTags('News')
@UseGuards(AuthGuard('jwt'))
@Controller('api/news')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @ApiOperation({ summary: 'Find news' })
  @ApiOkResponse({
    description: 'Find news',
    type: FindNewsResponseDTO,
    isArray: true
  })
  @Get()
  findNews(@Query() query: FindNewsParamsDTO): Promise<FindNewsResponseDTO[]> {
    return this.newsService.find(query);
  }

  @ApiOperation({ summary: 'Destroy new' })
  @ApiOkResponse({
    description: 'Destroy new',
  })
  @Delete(':id')
  destroyNew(@Param('id') id: number): Promise<void> {
    return this.newsService.destroy(id);
  }
}
