import { HttpService } from '@nestjs/axios';
import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { firstValueFrom } from 'rxjs';
import { Repository } from 'typeorm';

import { FindNewsParams } from './types/params/findNews.type';
import { New } from './new.entity';
import { FindNewsResponse } from './types/responses/findNews.type';

@Injectable()
export class NewsService {
  private MOTHS: string[] = [
    'JANUARY',
    'FEBRARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
  ];

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    @InjectRepository(New)
    private repository: Repository<New>,
  ) {}

  async retriveFromAPI(): Promise<void> {
    const { data } = await firstValueFrom(
      this.httpService.get(this.configService.get<string>('URL_HACKING_NEWS')),
    );

    if (!data?.hits) return;

    const { hits } = data;

    const filteredHits = hits.filter((hit) => hit?.objectID);
    const news = this.repository.create(filteredHits);
    await this.save(news);
  }

  async save(news: New[]): Promise<void> {
    const _news: New[] = [];

    for (const _new of news) {
      const { id, objectID } = _new;
      const findNew = await this.find({ id, objectID, withDeleted: true });

      if (!findNew.length) _news.push(_new);
    }

    await this.repository.save(_news);
  }

  async find({
    id,
    author,
    _tags,
    story_title,
    month,
    objectID,
    withDeleted,
    page,
  }: FindNewsParams): Promise<FindNewsResponse[]> {

    const queryBuilder = this.repository.createQueryBuilder('news')
      .select(`news.id,
      news.title,
      news.url,
      news.author,
      news.points,
      news.story_text,
      news.comment_text,
      news.num_comments,
      news.story_id,
      news.story_title,
      news.story_url,
      news.parent_id,
      news.created_at_i,
      news._tags,
      news.objectID,
      news.created_at`);

    if (id) queryBuilder.where('news.id = :id', { id });

    if (author) queryBuilder.andWhere('news.author = :author', { author });

    if (_tags) {
      queryBuilder.andWhere('news._tags @> Array[:..._tags]', {
        _tags: _tags.split(','),
      });
    }

    if (story_title)
      queryBuilder.andWhere('news.story_title = :story_title', { story_title });

    if (month)
      queryBuilder.andWhere(
        'EXTRACT(MONTH FROM news.created_at) = :moth_number',
        {
          moth_number: this.MOTHS.indexOf(month.toUpperCase()) + 1,
        },
      );

    if (objectID)
      queryBuilder.andWhere('news.objectID = :objectID', { objectID });

    if (withDeleted) queryBuilder.softDelete();

    return queryBuilder
      .take(5)
      .skip((page ? page - 1 : 0) * 5)
      .getRawMany();
  }

  async destroy(id: number): Promise<void> {
    const findNew = await this.find({ id });
    if (!findNew.length) throw new NotFoundException('New not found');
    await this.repository.softDelete({ id });
  }
}
