import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsDate,
  IsNumber,
  IsPositive,
  IsString,
} from 'class-validator';

export class FindNewsResponseDTO {
  @ApiProperty({
    type: Number,
    description: `News's id`,
  })
  @IsNumber()
  @IsPositive()
  id: number;

  @ApiProperty({
    type: String,
    description: `Title of the news`,
  })
  @IsDate()
  title: string;

  @ApiProperty({
    type: String,
    description: `News's url`,
  })
  @IsString()
  url: string;

  @ApiProperty({
    type: String,
    description: `Author of the news`,
  })
  @IsString()
  author: string;

  @ApiProperty({
    type: Number,
    description: `Points of the news`,
  })
  @IsNumber()
  @IsPositive()
  points: number;

  @ApiProperty({
    type: String,
    description: `Text of the story`,
  })
  @IsString()
  story_text?: string;

  @ApiProperty({
    type: String,
    description: `Comment text`,
  })
  @IsString()
  comment_text?: string;

  @ApiProperty({
    type: Number,
    description: `Quantity of comments`,
  })
  @IsNumber()
  @IsPositive()
  num_comments?: number;

  @ApiProperty({
    type: Number,
    description: `Id of the story`,
  })
  @IsNumber()
  @IsPositive()
  story_id?: number;

  @ApiProperty({
    type: String,
    description: `Title of the story`,
  })
  @IsString()
  story_title?: string;

  @ApiProperty({
    type: String,
    description: `Story's url.`,
  })
  @IsString()
  story_url?: string;

  @ApiProperty({
    type: Number,
    description: `New;s parent id`,
  })
  @IsString()
  parent_id?: number;

  @ApiProperty({
    type: Number,
    description: `Created at i`,
  })
  @IsNumber()
  @IsPositive()
  created_at_i?: number;

  @ApiProperty({
    type: [String],
    description: `Tags of the news`,
  })
  @IsArray()
  @IsString()
  _tags: string[];

  @ApiProperty({
    type: String,
    description: `Object ID`,
  })
  @IsString()
  objectID: string;

  @ApiProperty({
    type: Date,
    description: `Date of creation of the new`,
  })
  @IsDate()
  created_at: Date;
}

