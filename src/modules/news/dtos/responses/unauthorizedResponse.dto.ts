import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class UnauthorizedResponseDTO {
  @ApiProperty({
    description: 'Status code',
    default: 401,
  })
  @IsNumber()
  @IsNotEmpty()
  statusCode: number;

  @ApiProperty({
    description: 'Message',
    default: 'Unauthorized',
  })
  @IsString()
  @IsNotEmpty()
  message: string;
}
