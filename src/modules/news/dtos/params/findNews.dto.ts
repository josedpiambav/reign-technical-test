import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class FindNewsParamsDTO {

  @ApiPropertyOptional({
    description: 'Number of the page that the user is retrieving',
  })
  @IsOptional()
  @IsString()
  page?: number;

  @ApiPropertyOptional({
    description: 'Option to filter items by author',
  })
  @IsOptional()
  @IsString()
  author?: string;

  @ApiPropertyOptional({
    description: 'Option to filter items by _tags',
  })
  @IsOptional()
  @IsString()
  _tags?: string;

  @ApiPropertyOptional({
    description: 'Option to filter items by month',
  })
  @IsOptional()
  @IsString()
  month?: string;

  @ApiPropertyOptional({
    description: 'Option to filter items by story title',
  })
  @IsOptional()
  @IsString()
  story_title?: string;
}
