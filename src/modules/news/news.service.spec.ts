import { HttpService } from '@nestjs/axios';
import { NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestApplication } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import * as RXJS from 'rxjs';
import { New } from './new.entity';
import { NewsService } from './news.service';

describe('NewsService', () => {
  let app: NestApplication;
  let service: NewsService;

  const NewsMock = {
    id: 1,
    title: 'TEST_NEW',
    url: 'TEST_URL',
    author: 'TEST_AUTHOR',
    points: 1,
    story_text: 'TEST_STORY',
    comment_text: 'TEST_COMMENT',
    num_comments: 1,
    story_id: 1,
    story_title: 'TEST_STORY_TITLE',
    story_url: 'TEST_STORY_URL',
    parent_id: 1,
    created_at_i: 'TEST_CREATED',
    _tags: [],
    objectID: 'TEST_OBJECT_ID',
    created_at: 'TEST_CREATED',
  };

  const HttpServiceMock = {
    get: jest.fn(),
  };

  const ConfigServiceMock = {
    get: jest.fn((key: string) => {
      if (key === 'URL_HACKING_NEWS') {
        return 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
      }
      return null;
    })
  }

  const NewsRepositoryMock = {
    create: jest.fn(),
    save: jest.fn(),
    createQueryBuilder: jest.fn().mockReturnThis(),
    select: jest.fn().mockReturnThis(),
    where: jest.fn().mockReturnThis(),
    andWhere: jest.fn().mockReturnThis(),
    softDelete: jest.fn().mockReturnThis(),
    take: jest.fn().mockReturnThis(),
    skip: jest.fn().mockReturnThis(),
    getRawMany: jest.fn().mockResolvedValue([NewsMock]),
  };

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [],
      providers: [
        NewsService,
        {
          provide: HttpService,
          useValue: HttpServiceMock,
        },
        {
          provide: ConfigService,
          useValue: ConfigServiceMock
        },
        {
          provide: getRepositoryToken(New),
          useValue: NewsRepositoryMock,
        },
      ],
    }).compile();

    service = module.get<NewsService>(NewsService);

    app = module.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('retriveFromAPI', () => {
    it('Should call the right methods', async () => {
      jest.spyOn(RXJS, 'firstValueFrom').mockResolvedValue({
        data: {
          hits: [NewsMock],
        },
      });

      NewsRepositoryMock.create.mockReturnValue([NewsMock]);

      const saveSpy = jest.spyOn(service, 'save').mockImplementation();

      await service.retriveFromAPI();

      expect(HttpServiceMock.get).toHaveBeenLastCalledWith(
        ConfigServiceMock.get('URL_HACKING_NEWS'),
      );
      expect(NewsRepositoryMock.create).toHaveBeenLastCalledWith([NewsMock]);
      expect(saveSpy).toHaveBeenLastCalledWith([NewsMock]);
    });

    it('Should finish if hits is undefined', async () => {
      jest.spyOn(RXJS, 'firstValueFrom').mockResolvedValue({
        data: {},
      });

      await service.retriveFromAPI();

      expect(HttpServiceMock.get).toHaveBeenLastCalledWith(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      );
      expect(NewsRepositoryMock.create).not.toHaveBeenCalled();
    });

    it('Should finish if no data provided', async () => {
      jest.spyOn(RXJS, 'firstValueFrom').mockResolvedValue({});

      await service.retriveFromAPI();

      expect(HttpServiceMock.get).toHaveBeenLastCalledWith(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      );
      expect(NewsRepositoryMock.create).not.toHaveBeenCalled();
    });
  });

  describe('save', () => {
    it('Should call the right methods', async () => {
      const findSpy = jest.spyOn(service, 'find').mockResolvedValue([]);

      await service.save([NewsMock] as any);

      expect(findSpy).toHaveBeenCalledWith({
        id: NewsMock.id,
        objectID: NewsMock.objectID,
        withDeleted: true,
      });

      expect(NewsRepositoryMock.save).toHaveBeenCalledWith([NewsMock]);
    });
  });

  describe('find', () => {
    it('Should call the right methods', async () => {
      const response = await service.find({
        id: 1,
        author: 'TEST',
        _tags: 'TEST_TAG',
        story_title: 'TEST_STORY_TITLE',
        month: 'TEST_MONTH',
        objectID: 'TEST_OBJECT_ID',
        withDeleted: false,
        page: 1,
      });

      expect(response).toEqual([NewsMock]);

      expect(NewsRepositoryMock.createQueryBuilder).toHaveBeenCalledWith(
        'news',
      );
      expect(NewsRepositoryMock.select).toHaveBeenCalledWith(`news.id,
      news.title,
      news.url,
      news.author,
      news.points,
      news.story_text,
      news.comment_text,
      news.num_comments,
      news.story_id,
      news.story_title,
      news.story_url,
      news.parent_id,
      news.created_at_i,
      news._tags,
      news.objectID,
      news.created_at`);
      expect(NewsRepositoryMock.where).toHaveBeenCalledTimes(1);
      expect(NewsRepositoryMock.andWhere).toHaveBeenCalledTimes(5);
      expect(NewsRepositoryMock.softDelete).not.toHaveBeenCalled();
      expect(NewsRepositoryMock.take).toHaveBeenCalledWith(5);
      expect(NewsRepositoryMock.skip).toHaveBeenCalledWith(0);
      expect(NewsRepositoryMock.getRawMany).toHaveBeenCalledTimes(1);
    });

    it('Should call the right methods with soft delete', async () => {
      const response = await service.find({
        id: 1,
        withDeleted: true,
      });

      expect(response).toEqual([NewsMock]);

      expect(NewsRepositoryMock.createQueryBuilder).toHaveBeenCalledWith(
        'news',
      );
      expect(NewsRepositoryMock.select).toHaveBeenCalledWith(`news.id,
      news.title,
      news.url,
      news.author,
      news.points,
      news.story_text,
      news.comment_text,
      news.num_comments,
      news.story_id,
      news.story_title,
      news.story_url,
      news.parent_id,
      news.created_at_i,
      news._tags,
      news.objectID,
      news.created_at`);
      expect(NewsRepositoryMock.where).toHaveBeenCalledTimes(1);
      expect(NewsRepositoryMock.andWhere).not.toHaveBeenCalled();
      expect(NewsRepositoryMock.softDelete).toHaveBeenCalledTimes(1);
      expect(NewsRepositoryMock.take).toHaveBeenCalledWith(5);
      expect(NewsRepositoryMock.skip).toHaveBeenCalledWith(0);
      expect(NewsRepositoryMock.getRawMany).toHaveBeenCalledTimes(1);
    });
  });

  describe('destroy', () => {
    it('Should call the right methods', async () => {
      const findSpy = jest
        .spyOn(service, 'find')
        .mockResolvedValue([NewsMock] as any);

      await service.destroy(1);

      expect(findSpy).toHaveBeenCalledWith({ id: 1 });
      expect(NewsRepositoryMock.softDelete).toHaveBeenCalledWith({ id: 1 });
    });

    it('Should throw NotFoundException', async () => {
      const findSpy = jest.spyOn(service, 'find').mockResolvedValue([]);

      try {
        await service.destroy(1);
      } catch (error) {
        expect(findSpy).toHaveBeenCalledWith({ id: 1 });
        expect(error).toBeInstanceOf(NotFoundException);
        expect(error.message).toEqual('New not found');
      }
    });
  });
});
