import { NestApplication } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from './news.service';
import { HourlyRetriveNewsTask } from './news.task';

describe('HourlyRetriveNewsTask', () => {
  let app: NestApplication;
  let service: HourlyRetriveNewsTask;

  const NewsServiceMock = {
    retriveFromAPI: jest.fn(),
  };

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [],
      providers: [
        HourlyRetriveNewsTask,
        {
          provide: NewsService,
          useValue: NewsServiceMock,
        },
      ],
    }).compile();

    service = module.get<HourlyRetriveNewsTask>(HourlyRetriveNewsTask);

    app = module.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getHackerNews', () => {
    it('Should call the right methods', async () => {
      await service.getHackerNews();

      expect(NewsServiceMock.retriveFromAPI).toHaveBeenCalledTimes(1);
    });
  });
});
