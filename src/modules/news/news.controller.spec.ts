import * as request from 'supertest';
import { HttpStatus, ValidationPipe } from '@nestjs/common';
import { NestApplication } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import '@nestjs/passport';

jest.mock('@nestjs/passport', () => ({
  ...jest.requireActual<any>('@nestjs/passport'),
  AuthGuard: () => jest.fn(),
}));

describe('NewsController', () => {
  let app: NestApplication;
  let controller: NewsController;

  const NewsMock = {
    id: 1,
    title: 'TEST_NEW',
    url: 'TEST_URL',
    author: 'TEST_AUTHOR',
    points: 1,
    story_text: 'TEST_STORY',
    comment_text: 'TEST_COMMENT',
    num_comments: 1,
    story_id: 1,
    story_title: 'TEST_STORY_TITLE',
    story_url: 'TEST_STORY_URL',
    parent_id: 1,
    created_at_i: 'TEST_CREATED',
    _tags: [],
    objectID: 'TEST_OBJECT_ID',
    created_at: 'TEST_CREATED',
  };

  const NewsServiceMock = {
    find: jest.fn(),
    destroy: jest.fn(),
  };

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [NewsController],
      providers: [
        {
          provide: NewsService,
          useValue: NewsServiceMock,
        },
      ],
    }).compile();

    controller = module.get<NewsController>(NewsController);

    app = module.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('GET /api/news', () => {
    it('Should call the right methods', async () => {
      NewsServiceMock.find.mockResolvedValue([NewsMock]);

      const response = await request(app.getHttpServer())
        .get('/api/news')
        .send();

      expect(response.status).toEqual(HttpStatus.OK);
      expect(response.body).toEqual([NewsMock]);
      expect(NewsServiceMock.find).toHaveBeenCalledWith({});
    });
  });

  describe('DELETE /api/news/:id', () => {
    it('Should call the right methods', async () => {
      NewsServiceMock.find.mockResolvedValue([NewsMock]);

      const response = await request(app.getHttpServer())
        .delete('/api/news/1')
        .send();

      expect(response.status).toEqual(HttpStatus.OK);
      expect(NewsServiceMock.destroy).toHaveBeenCalledWith('1');
    });
  });
});
