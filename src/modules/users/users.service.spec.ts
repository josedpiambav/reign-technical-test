import { BadRequestException } from '@nestjs/common';
import { NestApplication } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import * as Bcrypt from 'bcryptjs';
import { User } from './user.entity';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let app: NestApplication;
  let service: UsersService;

  const UserMock = {
    id: 1,
    username: 'TEST',
    password: 'TEST',
  };

  const UserRepositoryMock = {
    create: jest.fn(),
    save: jest.fn(),
    find: jest.fn(),
  };

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [],
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: UserRepositoryMock,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);

    app = module.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('save', () => {
    it('Should call the right methods', async () => {
      const findSpy = jest.spyOn(service, 'find').mockResolvedValue([]);

      jest.spyOn(Bcrypt, 'hashSync').mockReturnValue(UserMock.password);

      UserRepositoryMock.create.mockReturnValue(UserMock);

      await service.save({
        username: UserMock.username,
        password: UserMock.password,
      });

      expect(findSpy).toHaveBeenCalledWith({ username: UserMock.username });
      expect(UserRepositoryMock.create).toHaveBeenCalledWith({
        username: UserMock.username,
        password: UserMock.password,
      });
      expect(UserRepositoryMock.save).toHaveBeenCalledWith(UserMock);
    });

    it('Should throw BadRequestException', async () => {
      const findSpy = jest.spyOn(service, 'find').mockResolvedValue([UserMock]);

      try {
        await service.save({
          username: UserMock.username,
          password: UserMock.password,
        });
      } catch (error) {
        expect(findSpy).toHaveBeenCalledWith({ username: UserMock.username });
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toEqual('User already exists');
      }
    });
  });

  describe('find', () => {
    it('Should call the right methods', async () => {
      UserRepositoryMock.find.mockResolvedValue([UserMock]);

      const response = await service.find({
        id: UserMock.id,
        username: UserMock.username,
      });

      expect(response).toEqual([UserMock]);
      expect(UserRepositoryMock.find).toHaveBeenCalledWith({
        where: {
          id: UserMock.id,
          username: UserMock.username,
        },
      });
    });
  });

  describe('verifyPassword', () => {
    it('Should call the right methods', () => {
      const compareSyncSpy = jest
        .spyOn(Bcrypt, 'compareSync')
        .mockReturnValue(true);

      const response = service.verifyPassword(UserMock, 'TEST');

      expect(response).toEqual(true);
      expect(compareSyncSpy).toHaveBeenCalledWith('TEST', UserMock.password);
    });
  });
});
