export type FindUser = {
  id?: number;
  username?: string;
};
