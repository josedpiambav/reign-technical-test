import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';
import { Repository } from 'typeorm';

import { CreateUser } from './types/createUser.type';
import { FindUser } from './types/findUser.type';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private repository: Repository<User>,
  ) {}

  async save({ username, password }: CreateUser): Promise<void> {
    const userExists = await this.find({ username });

    if (userExists.length) throw new BadRequestException('User already exists');

    const user = this.repository.create({
      username,
      password: bcrypt.hashSync(password, bcrypt.genSaltSync()),
    });

    await this.repository.save(user);
  }

  async find({ id, username }: FindUser): Promise<User[]> {
    let where = {};

    if (id) where = { ...where, id };
    if (username) where = { ...where, username };
    return this.repository.find({ where });
  }

  verifyPassword(user: User, password: string): boolean {
    return bcrypt.compareSync(password, user.password);
  }
}
