import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';

import config from './common/config';
import validationSchema from './common/config/validationSchema.config';
import { TypeOrmConfigService } from './common/database/typeorm.service';
import { AuthModule } from './common/auth/auth.module';

import { NewsModule } from './modules/news/news.module';

@Module({
  imports: [
    ConfigModule.forRoot({ load: [config], isGlobal: true, validationSchema }),
    ScheduleModule.forRoot(),
    TypeOrmModule.forRootAsync({ useClass: TypeOrmConfigService }),
    AuthModule,
    NewsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
