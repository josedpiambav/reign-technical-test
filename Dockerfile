FROM node:16.17.0 AS base

FROM base AS deps

WORKDIR /deps
COPY ./package* ./
COPY ./yarn* ./
RUN yarn install --frozen-lockfile && yarn cache clean --all

FROM base AS builder

WORKDIR /builder
COPY --from=deps /deps/node_modules ./node_modules
COPY ./src ./
COPY ./package* ./
COPY ./yarn* ./
COPY ./tsconfig* ./
RUN yarn run build

FROM base AS app

WORKDIR /app
COPY --from=deps /deps/node_modules ./node_modules
COPY --from=builder /builder/dist ./dist
COPY .env .
COPY ./package.json ./
USER node
EXPOSE 3000
CMD ["yarn", "run", "start:prod"]
